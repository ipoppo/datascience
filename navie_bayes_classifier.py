#%%
import numpy as np
import pandas as pd

# Create an empty dataframe
##### Train Set #####
data = pd.DataFrame()

# Create our target variable
data['Gender'] = ['male','male','male','male','female','female','female','female']

# Create our feature variables
data['Height'] = [6,5.92,5.58,5.92,5,5.5,5.42,5.75]
data['Weight'] = [180,190,170,165,100,150,130,150]
data['Foot_Size'] = [12,11,12,10,6,8,7,9]

# View the data
data

#%%
##### Test Set #####
person = pd.DataFrame()

# Create some feature values for this single row
person['Height'] = [6]
person['Weight'] = [130]
person['Foot_Size'] = [8]

# View the data 
person

#%%

# Counting Groups
n_male = data['Gender'][data['Gender']=='male'].count()
n_female = data['Gender'][data['Gender']=='female'].count()
n_all = n_male + n_female

p_male = n_male/n_all
p_female = n_female/n_all

# Group the data by gender and calculate the means of each feature
data_means = data.groupby('Gender').mean()

# View the values
data_means

# Group the data by gender and calculate the variance of each feature
data_vars = data.groupby('Gender').var()

# View the values
data_vars

#%%

#male_height_mean = data_means['Height'][data_means.index=='male'].values[0]
#male_weight_mean = data_means['Weight'][data_means.index=='male'].values[0]
#male_footsize_mean = data_means['Foot_Size'][data_means.index=='male'].values[0]

#male_height_var = data_vars['Height'][data_vars.index=='male'].values[0]
#male_weight_var = data_vars['Weight'][data_vars.index=='male'].values[0]
#male_footsize_var = data_vars['Foot_Size'][data_vars.index=='male'].values[0]

#female_height_mean = data_means['Height'][data_means.index=='female'].values[0]
#female_weight_mean = data_means['Weight'][data_means.index=='female'].values[0]
#female_footsize_mean = data_means['Foot_Size'][data_means.index=='female'].values[0]

#female_height_var = data_vars['Height'][data_vars.index=='female'].values[0]
#female_weight_var = data_vars['Weight'][data_vars.index=='female'].values[0]
#female_footsize_var = data_vars['Foot_Size'][data_vars.index=='female'].values[0]

def naivebayse_gaussian(x, predict, mean_df, var_df):
    p = 1
    for f in mean_df:
        mean_y = mean_df[f][mean_df.index==predict].values[0]
        variance_y = var_df[f][var_df.index==predict].values[0]
        p *= np.exp(-(x[f][0]-mean_y)**2/(2*variance_y)) / np.sqrt(2*np.pi*variance_y)
    return p

#%%

# P(male|y) = P(male).P(y|male)= P(male).P(height|male).P(weight|male).P(footsize|male)

x = person

p_is_male = p_male * naivebayse_gaussian(person, 'male', data_means, data_vars)
#P_male * \
#p_x_given_y(person['Height'][0], male_height_mean, male_height_variance) * \
#p_x_given_y(person['Weight'][0], male_weight_mean, male_weight_variance) * \
#p_x_given_y(person['Foot_Size'][0], male_footsize_mean, male_footsize_variance)

print('P(x=male|y)=', p_is_male)

p_is_female = p_female * naivebayse_gaussian(person, 'female', data_means, data_vars)
#P_female * \
#p_x_given_y(person['Height'][0], female_height_mean, female_height_variance) * \
#p_x_given_y(person['Weight'][0], female_weight_mean, female_weight_variance) * \
#p_x_given_y(person['Foot_Size'][0], female_footsize_mean, female_footsize_variance)

print('P(x=female|y)=', p_is_female)

if (p_is_female > p_is_male):
    print('Likely to be female')
else:
    print('Likely to be male')

