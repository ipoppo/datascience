#%%
import numpy as np
import pandas as pd

# Read Data
df = pd.read_csv('creditcard.csv')
df_size = df.shape[0]

#%%
# Split data into train set/validation set
SPLIT_RATIO = 0.8
df['rng'] = np.random.rand(df_size)
df_train = df[df['rng'] <= SPLIT_RATIO]
df_train = df_train[df_train.columns.drop(['rng', 'Time'])]
df_train = df_train.reindex(np.random.permutation(df_train.index))
df_test = df[df['rng'] > SPLIT_RATIO]
df_test = df_test[df_test.columns.drop(['rng', 'Time'])]
df_test = df_test.reindex(np.random.permutation(df_test.index))

stat_train_normal = df_train[df_train['Class'] == 0].shape[0]
stat_train_fruad = df_train[df_train['Class'] == 1].shape[0]
stat_test_normal = df_test[df_test['Class'] == 0].shape[0]
stat_test_fruad = df_test[df_test['Class'] == 1].shape[0]
print('Train set statistics: {:d}/{:d}'.format(stat_train_normal, stat_train_fruad))
print('Test set statistics: {:d}/{:d}'.format(stat_test_normal, stat_test_fruad))

#%%
from sklearn.neighbors import KNeighborsClassifier

def df_to_input(df):
    return df[df.columns.drop('Class')], df['Class']

X_train, y_train = df_to_input(df_train)

knn = KNeighborsClassifier()
knn.fit(X_train, y_train)

#%%
# The meat
from sklearn.metrics import precision_recall_fscore_support

X_test, y_truth = df_to_input(df_test)
y_predict = knn.predict(X_test)

#%%
# Evaluation
precision_recall_fscore_support(y_truth, y_predict, average='macro')

# My result of single run is
# precision_recall_fscore_support(y_truth, y_predict, average='macro') = (0.9765713481762865, 0.8038951717950709, 0.8710683692999095, None)

