#%%
import numpy as np
import pandas as pd

# Read Data
df = pd.read_csv('creditcard.csv')
df_size = df.shape[0]

#%%
# Split data into train set/validation set
SPLIT_RATIO = 0.8
df['rng'] = np.random.rand(df_size)
df_train = df[df['rng'] <= SPLIT_RATIO]
df_train = df_train[df_train.columns.drop(['rng', 'Time'])]
df_train = df_train.reindex(np.random.permutation(df_train.index))
df_test = df[df['rng'] > SPLIT_RATIO]
df_test = df_test[df_test.columns.drop(['rng', 'Time'])]
df_test = df_test.reindex(np.random.permutation(df_test.index))

stat_train_normal = df_train[df_train['Class'] == 0].shape[0]
stat_train_fruad = df_train[df_train['Class'] == 1].shape[0]
stat_test_normal = df_test[df_test['Class'] == 0].shape[0]
stat_test_fruad = df_test[df_test['Class'] == 1].shape[0]
print('Train set statistics: {:d}/{:d}'.format(stat_train_normal, stat_train_fruad))
print('Test set statistics: {:d}/{:d}'.format(stat_test_normal, stat_test_fruad))

#%%
from keras.utils import to_categorical

# Neural Network is strict about shape of input 😇
def df_to_binary(df):
    x = df[df.columns.drop('Class')].values
    y = np.reshape(df['Class'].values, (-1,1))
    return x, y

def df_to_onehot(df):
    x = df[df.columns.drop('Class')].values
    y = to_categorical(df['Class'].values)
    return x, y


X_train, y_train = df_to_binary(df_train)
num_features = X_train.shape[1]
print ('Train set np.shape:', X_train.shape)
print ('Train label np.shape:', y_train.shape)

#%%
from keras.models import Sequential
from keras.layers import Dense, Activation

model = Sequential()
model.add(Dense(20, activation='relu', input_dim=num_features))
model.add(Dense(20, activation='relu'))

# binary, [loss='binary_crossentropy']
model.add(Dense(1, activation='sigmoid'))

# one_hot_model, [loss='categorical_crossentropy']
#model.add(Dense(2, activation='softmax'))


model.compile(optimizer='rmsprop', 
              loss='binary_crossentropy', 
              metrics=['accuracy'])

model.fit(X_train, y_train, epochs=2, batch_size=32)
print ('Model training completed!')

#%%
# Evaluation
X_test, y_truth = df_to_binary(df_test)
score = model.evaluate(X_test, y_truth, batch_size=128)
print('Model accuracy with test set:', score[1])

# My result of single run is
# loss, accuracy = [0.028880011305166643, 0.9982082316220725]
